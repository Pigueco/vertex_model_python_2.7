# README #

This folder contain the files you need to run vertex model with inter-kinetic nucleus movement describes in \cite{paper}. This program has been used to represent the neural tube dynamic.

### About Vertex model ###

Vertex model can be used to describe the pseudo-stratified epithelium of the neural tube. Vertex models have been used to study the epithelial junctional network in the Drosophila imaginal disc *Landsberg2009, Farhadifar2007 and Aegerter-Wilmsen2010* and various other tissues. *Fletcher2014 and Honda2015* review vertex models and their application to epithelial morphogenesis. 


Cells are represented as polygons with straight edges connecting vertices. The evolution of each cell in these models is governed by the motion of its vertices, which are typically assumed to obey deterministic equations of motion. The evolution of the position r_i of vertex i determined by movement eq.:

```
mu dr_i/dt= F_i(t)
```
where F_i(t) denotes the total force acting on vertex i at time t and mu denotes its drag coefficient. The main difference between the models lies in the definition of the force $F_i$ that can be derived from an energy function, $E_i$, that includes the different cell-cell interactions. In our model we will use a modification of the energy described in *Farhadifar2007* including a modification in the target area. 




### Requirements ###
The code is tested on Linux Mint 16.04. requires different tools and libraries especific versions 

* Python: 2.7
* matplotlib.__version__numpy__: '1.11.1'
* matplotlib.__version__: 2.0.0
* numba: 0.24.0


### Building ###

Download the folder *vertex_model*, and files *setup.py* and *Makefile* everything in the same directory. In the folder containing *Makefile* file and all libraries, once you are in the folder you need to type in a terminal:
```
make
```

### How do I get set up? ###

```python



#simulation without division
def basic_simulation

# simulation with division and INM (no differentiation rate domain)
def simulation_with_division

# simulation with division, INM following clones (no differentiation rate domain)
def simulation_with_division_clone

# simulation with division and all the cell are differentated as pNM 
def simulation_with_division_clone_differentiation

# simulation with division with INM and 2 diferent populations (with and without differentiation rate, pD and pMN) 
def simulation_with_division_clone_differenciation_3stripes




def run_simulation_INM(x, timend,rand, sim_type):
    #sim_type 0 simulation_with_division_clone (no differentiation rate)
    #sim_type 1 simulation_with_division_clone_differentiation (all tissue with differentiation rate)
    #sim_type 2 simulation_with_division_clone_differenciation_3stripes (2 population with and without diffentiation rate)

    K=x[0]
    G=x[1]
    L=x[2]
```
