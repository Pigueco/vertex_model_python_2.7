
# coding: utf-8

# In[6]:

import itertools
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from IPython.display import HTML
import vertex_model as model
from vertex_model.run_select import run_simulation_INM, definecolors


# In[50]:

#### Parameter used in the simulation are picked up form Gobal_Constant.py file

### Choose simulation to run
#def run_simulation_INM(x, timend,rand, type):
    #sim_type 0 simulation_with_division_clone (no differentiation rate)
    #sim_type 1 simulation_with_division_clone_differentiation (all differentiation rate)
    #sim_type 2 simulation_with_division_clone_differenciation_3stripes (2 population with and without diffentiation rate)
    #sim_type 3 simulation_with_division_clone_whole_tissue_differenciation (differentiation rate everywhere)
type_=0
L_point = [-.4, -0.2,-0.3, -0.05, 0.075, 0.15]
G_point = [0.14, 0.12, 0.1, 0.065, 0.04, 0.02]
i=4
G=G_point[i]
L=L_point[i]
K=1.0
#run simulation with the choosen parameters
rand =  np.random.RandomState() #random number to choose Lambda
params = [K,G,L]  # K=x[0],G=x[1],L=x[2]
history= run_simulation_INM(params,100,rand,type_) #return hist




# In[69]:

def definecolors(cells, sampleset):
    peach = '#eed5b7'
    light_blue ='#87cefa'
    pink = '#ffc0cb'
    light_green = '#98fb98'
    palette = np.array([peach, 'r', peach,light_green,light_blue,light_green])
#     colors = cells.properties['parent_group']
    colors = cells.properties['parent_group']+np.array([3 if x in sampleset else 0 for x in cells.properties['parent']])

    return palette[colors]
pd_0=np.random.choice(np.where((history[0].properties['parent_group']==0))[0], 4)
pd_2=np.random.choice(np.where((history[0].properties['parent_group']==2))[0], 4)
pmn=np.random.choice(np.where((history[0].properties['parent_group']==1))[0], 4)

sampleset =np.append(np.append(pd_0,pd_2),pmn)

for cells in history:
    cells.properties['color'] = definecolors(cells,sampleset)
# cells.properties['color']=cmap(norm(color_face))


# In[70]:



fig=plt.figure()
ax = fig.gca()
# initialization function: plot the background of each frame
def init_fig():
    ax = plt.figure()
    return (ax,)
# animation function. This is called sequentially
def animate_fig(i):
    cells_array=history
    v_max = np.max((np.max(cells_array[0].mesh.vertices), np.max(cells_array[-1].mesh.vertices)))
    size = 2.0*v_max
    cells= history[i]
    return model.draw(cells,ax,size)
# call the animator. blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig,animate_fig, init_func=init_fig,
                               frames=len(history))


# In[71]:

HTML(anim.to_html5_video())


# In[ ]:



